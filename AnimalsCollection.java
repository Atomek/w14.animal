package animal;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AnimalsCollection<T extends Animal> {
    static Scanner scanner = new Scanner(System.in);
    static List<Animal> listAnimals = new ArrayList<>();

    public static void main(String[] args) {
        menu();
    }
    private static void menu() {
        int choice;

        do {
            System.out.println("1.Dodaj zwierze \n2.Usuń \n3.moveAll() \n0.wyjdz");
            choice = scanner.nextInt();
            yourChoice(choice);
        } while (choice != 0);
    }

    private static void yourChoice(int choice) {
        switch (choice) {
            case 1:
                addAnimal();
                break;
            case 2:
                deleatAnimal();
                break;
            case 3:
                moveAll();
                break;
        }
    }

    private static void moveAll() {
        for (int i = 0; i < listAnimals.size(); i++) {
            listAnimals.get(i).move();
        }
    }

    private static void deleatAnimal() {
        System.out.println("Jakie zwierze chcesz usunać?");
        int choice = scanner.nextInt();
        listAnimals.remove(choice);
    }

    private static void addAnimal() {
        System.out.println("Dog or Cat");
        String whatKindOfAnimal = scanner.next();
        if (whatKindOfAnimal.equals("Cat")) {
            System.out.println("Name: ");
            String catName = scanner.next();
            Cat cat = new Cat(catName);
            listAnimals.add(cat);
        } else {
            System.out.println("Name: ");
            String dogName = scanner.next();
            Dog dog = new Dog(dogName);
            listAnimals.add(dog);
        }
    }


}
